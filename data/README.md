# Data files

## relationships_w_pops_121708.txt

This file contains information on all the individuals collected by HapMap.
It was downloaded on 2014-06-03, but has not been edited since 2008-12-07.
We use it for determining the relationship structure of the Yoruba trios.

```
wget http://hapmap.ncbi.nlm.nih.gov/downloads/samples_individuals/relationships_w_pops_121708.txt
md5sum relationships_w_pops_121708.txt
a70333f0e0bd9d5a72a3a253895228b1  relationships_w_pops_121708.txt
```

The file contains seven columns:

  + FID - family ID, e.g. Y004
  + IID - individual ID, e.g. NA18500
  + dad - IID of child's father
  + mom - IID of child's mother
  + sex - 1 = male, 2 = female
  + pheno - ?? They are all zero
  + population - The abbreviated name of the population of the individual, e.g. YRI for Yoruba in Ibadan, Nigeria

