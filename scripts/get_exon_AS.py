import sys,glob,pdb

class SNP:
    def __init__(self, line):
        info=line.strip().split()
        self.chrm=info[0]
        self.pos=int(info[1])
        self.allele1=info[2]
        self.allele2=info[3]
        self.parenta1=info[4]
        self.counta1=int(info[5])
        self.counta2=int(info[6])

exonfile=open("/mnt/lustre/data/share/Trios/Data_for_analysis/exon_counts_parents_noXYM.sort.txt")
outfile=open("exon_AS.txt",'w')
labels = exonfile.readline().strip().split()

bed_list=glob.glob("/mnt/lustre/data/share/Trios/Data_for_analysis/AS_counts/*_AS_info.bed")

first_line="chr\tpos\tpos\tENS"
family_ids=[]
parent_cols=[]
AS_lines=[]
AS_beds=[]
for fam in bed_list:
    fathercol=0
    mothercol=0
    ids=fam.strip().split("/")[-1].split('_')
    child=ids[0]
    father=ids[1]
    mother=ids[2]
    for col in range(len(labels)):
        if labels[col]==father:
            fathercol=col
        if labels[col]==mother:
            mothercol=col
    if(mothercol!=0 and fathercol!=0):
        family_ids.append((child,father,mother))
        parent_cols.append((fathercol,mothercol))
        newfile=open(fam)
        AS_lines.append(SNP(newfile.readline()))
        AS_beds.append(newfile)
        first_line+="\t%s_SNP_pos\tGeno_Dad\tGeno_Mom\tASDad\tASMom\tCountsDad\tCountsMom" %child

outfile.write(first_line+"\n")
outfile.flush()

line = exonfile.readline()
while line:
    exon_counts=line.strip().split()
    exon_chrm=exon_counts[0]
    exon_start=int(exon_counts[1])
    exon_end=int(exon_counts[2])

    outline="%s\t%s\t%s\t%s" % (exon_counts[0],exon_counts[1],exon_counts[2],exon_counts[3])
    tothits=0
    for i in range(len(AS_beds)):
        counts=["",""]
        hits=0
        snp_pos=""
        geno_Dad=""
        geno_Mom=""
        while (AS_lines[i].chrm==exon_chrm and AS_lines[i].pos<exon_end):
                    #assign allele specificity to reads that overlap our current SNP
            if exon_start<=AS_lines[i].pos:
                if(hits!=0):
                    snp_pos+=","
                    geno_Dad+=","
                    geno_Mom+=","
                    counts[0]+=","
                    counts[1]+=","
                hits+=1
                tothits+=1
                snp_pos+=str(AS_lines[i].pos)
                if AS_lines[i].parenta1 == "Dad":
                    counts[0]+=str(AS_lines[i].counta1)
                    counts[1]+=str(AS_lines[i].counta2)
                    geno_Dad+=AS_lines[i].allele1
                    geno_Mom+=AS_lines[i].allele2

                elif AS_lines[i].parenta1 == "Mom":
                    counts[0]+=str(AS_lines[i].counta2)
                    counts[1]+=str(AS_lines[i].counta1)
                    geno_Dad+=AS_lines[i].allele2
                    geno_Mom+=AS_lines[i].allele1
                else:
                    raise ValueError
            AS_lines[i]=SNP(AS_beds[i].readline())
        if hits==0:
            snp_pos="NA"
            geno_Dad="NA"
            geno_Mom="NA"
            counts=["NA","NA"]
        outline="%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (outline,snp_pos,geno_Dad,geno_Mom,counts[0],counts[1],exon_counts[parent_cols[i][0]],exon_counts[parent_cols[i][1]])
    if tothits>0:
        outfile.write(outline+"\n")
        outfile.flush()
    line = exonfile.readline()
    
